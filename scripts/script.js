//menu burger

const burgerOpen = document.querySelector(".burger_bar");
const burgerMenu = document.querySelector(".burger_menu");
const burgerClose = document.querySelector(".burger_close");

burgerOpen.addEventListener("click", () => {
    burgerMenu.style.transform = "translateX(0%)";
    burgerOpen.style.display = "none";
})

burgerClose.addEventListener("click", () => {
    burgerMenu.style.transform = "translateX(-100%)";
    burgerOpen.style.display = "block";
})


// formulaire

let showParticulier = document.querySelector(".showParticulier");
let showProfessionnel = document.querySelector(".showProfessionnel");
const radioParticulier = document.querySelector("#choixParticulier");
const radioProfessionnel = document.querySelector("#choixProfessionnel");

radioParticulier.addEventListener("click", () => {
    showParticulier.classList.remove("hidden");
    showProfessionnel.classList.add("hidden");
})

radioProfessionnel.addEventListener("click", () => {
    showProfessionnel.classList.remove("hidden");
    showParticulier.classList.add("hidden");
})


